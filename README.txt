# URLs of some fasta files
`http://ftp.ensembl.org/pub/release-105/fasta/naja_naja/cdna/Naja_naja.Nana_v5.cdna.all.fa.gz`
`http://ftp.ensembl.org/pub/release-105/fasta/ciona_intestinalis/cdna/Ciona_intestinalis.KH.cdna.all.fa.gz`

# Singularity containers

`https://depot.galaxyproject.org/singularity/seqkit%3A2.1.0--h9ee0642_0`
`https://depot.galaxyproject.org/singularity/prodigal%3A2.6.3--h779adbc_3`
`https://depot.galaxyproject.org/singularity/emboss%3A6.6.0--hf7d6862_3`

# nf-core running example

```shell
echo SAMEA104654416 > ids.txt

nextflow run nf-core/fetchngs --input ids.txt -profile singularity
```
